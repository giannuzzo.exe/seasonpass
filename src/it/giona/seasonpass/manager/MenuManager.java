package it.giona.seasonpass.manager;

import it.giona.seasonpass.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import javax.annotation.Nullable;
import java.util.List;

public class MenuManager {

    private Main plugin;

    public MenuManager(Main p){
        this.plugin = p;
    }

    public ItemStack createItem(String itemSection, @Nullable Player p){
        Material m;
        try{
            String configMaterial = plugin.getConfig().getString("Home.Items." + itemSection  +".Material", "BARRIER");
            m = Material.valueOf(configMaterial);
        } catch(IllegalArgumentException ex){
            m = Material.BARRIER;
            plugin.getLogger().severe("Material non trovato!!");
        }

        short data = (short) plugin.getConfig().getInt("Home.Items." + itemSection  +".Data", 0);

        int amount = plugin.getConfig().getInt("Home.Items." + itemSection  +".Amount", 1);

        ItemStack item = new ItemStack(m, amount, data);
        ItemMeta meta = item.getItemMeta();

        String name = plugin.getConfig().getString("Home.Items." + itemSection  +".Name").replace("&", "§");
        meta.setDisplayName(name);

        List<String> lore = plugin.getConfig().getStringList("Home.Items." + itemSection  +".Lore");

        for(int i = 0; i < lore.size(); i++) {
            String tmp;
            if(plugin.getPlayerData().getPassLevel(p) == plugin.getPassManager().maxLevel){
                tmp = lore.get(i).replaceAll("%name%", p.getName()).replaceAll("%level%", plugin.getLang().getConfig().getString("Items.Status.Completed")).replaceAll("%stars%", String.valueOf(Main.getInstance().getPlayerData().getStars(p)));

            }else{
                tmp = lore.get(i).replaceAll("%name%", p.getName()).replaceAll("%level%", String.valueOf(plugin.getPlayerData().getPassLevel(p))).replaceAll("%stars%", String.valueOf(Main.getInstance().getPlayerData().getStars(p)));
            }
            lore.set(i, tmp);
        }
        meta.setLore(plugin.taccList(lore));

        if(item.getType() == Material.SKULL_ITEM && p != null){
            SkullMeta sm = (SkullMeta) meta;
            sm.setOwner(p.getName());
        }

        item.setItemMeta(meta);
        return item;
    }

    public int getSlot(int x, int y){
        return x - 1 + 9 * (y - 1);
    }

    public Inventory getMenuInventory(Player p){
        Inventory inv = plugin.getServer().createInventory(p, plugin.getConfig().getInt("Home.Size"), plugin.getConfig().getString("Home.Name").replace("&", "§"));
        inv.setItem(plugin.getConfig().getInt("Home.Items.Stats.Slot"), createItem("Stats", p));
        inv.setItem(plugin.getConfig().getInt("Home.Items.Pass.Slot"), createItem("Pass", p));
        inv.setItem(plugin.getConfig().getInt("Home.Items.Challenges.Slot"), createItem("Challenges", p));
        return inv;
    }
}
