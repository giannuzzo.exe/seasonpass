package it.giona.seasonpass.listener;

import it.giona.seasonpass.Main;
import it.giona.seasonpass.seasonpass.PassLevel;
import it.giona.seasonpass.manager.PassManager;
import it.giona.seasonpass.item.ItemPack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class PassListener implements Listener {

    private Main plugin;

    public PassListener(Main p){
        this.plugin = p;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e){

        if(e.getWhoClicked() instanceof Player){
            Player p = (Player) e.getWhoClicked();
            Inventory inv = e.getClickedInventory();
            if(p != null){
                if(inv != null){
                    if(inv.getTitle() != null){
                        if(inv.getTitle().contains(("(Preview)"))){
                            e.setCancelled(true);
                            return;
                        }

                        if(inv.getTitle().equals(plugin.getPass().getConfig().getString("Pass.Name").replaceAll("&", "§").replaceAll("%page%", String.valueOf(plugin.getPassManager().getPlayerIndex(p))))){
                            e.setCancelled(true);
                            ItemStack item = e.getClickedInventory().getItem(e.getSlot());
                            if(item != null){
                                PassLevel clicked = null;
                                for(PassLevel l : plugin.getPassManager().getLevels()){
                                    if(item.isSimilar(l.getGuiItem())){
                                        clicked = l;
                                    }
                                }

                                if(clicked != null && !plugin.getConfig().getBoolean("advancement.automatic")){
                                    if(clicked.canBuy(p)){
                                        if(clicked.isNextLevel(p)){
                                            if(hasInventorySpace(p, clicked)){
                                                plugin.getPlayerData().removeStars(p, clicked.getPrice());
                                                plugin.getPassManager().addLevel(p, false);
                                                p.closeInventory();
                                            }else{
                                                p.closeInventory();
                                                p.sendMessage(plugin.getLang().getConfig().getString("Messages.Error.NoSpace").replaceAll("&", "§"));
                                            }
                                        }else{
                                            p.closeInventory();
                                            p.sendMessage(plugin.getLang().getConfig().getString("Messages.Error.Blocked").replaceAll("&", "§"));
                                        }
                                    }
                                } else {
                                    if(item.getType() == Material.SKULL_ITEM){
                                        SkullMeta meta = (SkullMeta) item.getItemMeta();
                                        if(meta.getOwner().equals(PassManager.ArrowItem.RIGHT.getOwner())){
                                            p.openInventory(plugin.getPassManager().getInventory(p, plugin.getPassManager().getPlayerIndex(p) + 1, false));
                                        } else if(meta.getOwner().equals(PassManager.ArrowItem.LEFT.getOwner())){
                                            p.openInventory(plugin.getPassManager().getInventory(p, plugin.getPassManager().getPlayerIndex(p) - 1, false));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private int getNeededSlots(Player p, PassLevel l){
        int needed = 0;
        for (ItemPack ip : l.getItemPacks()) {
            needed++;
            for (ItemStack item : p.getInventory().getContents()) {
                if (item != null && item.isSimilar(ip.getItem())) {
                    if (item.getAmount() + ip.getItem().getAmount() <= ip.getItem().getMaxStackSize()) {
                        needed--;
                    }
                }
            }
        }

        return needed;
    }

    private int getFreeSlots(Player p){
        int free = 0;
        for (ItemStack item : p.getInventory().getContents()) {
            if (item == null) {
                free++;
            }
        }
        return free;
    }

    private boolean hasInventorySpace(Player p, PassLevel l) {
        int needed = getNeededSlots(p, l);
        int free = getFreeSlots(p);

        return free >= needed;
    }
}