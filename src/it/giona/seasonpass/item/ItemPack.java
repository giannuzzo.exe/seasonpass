package it.giona.seasonpass.item;


import org.bukkit.inventory.ItemStack;

public class ItemPack {

    private String identifier;
    private ItemStack item;

    public ItemPack(String identifier, ItemStack item){
        this.identifier = identifier;
        this.item = item;
    }

    public String getIdentifier(){
        return this.identifier;
    }

    public ItemStack getItem(){
        return this.item.clone();
    }
}
